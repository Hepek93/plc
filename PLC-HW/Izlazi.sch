EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Relay:G5V-1 K?
U 1 1 5F82BBFD
P 4020 1540
AR Path="/5F82BBFD" Ref="K?"  Part="1" 
AR Path="/5F828554/5F82BBFD" Ref="K1"  Part="1" 
F 0 "K1" H 4450 1586 50  0000 L CNN
F 1 "G5V-1" H 4450 1495 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5150 1510 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4020 1540 50  0001 C CNN
	1    4020 1540
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F82BC03
P 5380 1520
AR Path="/5F82BC03" Ref="J?"  Part="1" 
AR Path="/5F828554/5F82BC03" Ref="OUT1"  Part="1" 
F 0 "OUT1" H 5460 1562 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 5460 1471 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 5380 1520 50  0001 C CNN
F 3 "~" H 5380 1520 50  0001 C CNN
	1    5380 1520
	1    0    0    -1  
$EndComp
Wire Wire Line
	4320 1240 4760 1240
Wire Wire Line
	4760 1240 4760 1520
Wire Wire Line
	4760 1520 5180 1520
Wire Wire Line
	4120 1240 4120 1130
Wire Wire Line
	4120 1130 4840 1130
Wire Wire Line
	4840 1130 4840 1420
Wire Wire Line
	4840 1420 5180 1420
Wire Wire Line
	5180 1620 4760 1620
Wire Wire Line
	4760 1620 4760 1880
Wire Wire Line
	4760 1880 4220 1880
Wire Wire Line
	4220 1880 4220 1840
$Comp
L Relay:G5V-1 K?
U 1 1 5F82DE99
P 4040 3040
AR Path="/5F82DE99" Ref="K?"  Part="1" 
AR Path="/5F828554/5F82DE99" Ref="K2"  Part="1" 
F 0 "K2" H 4470 3086 50  0000 L CNN
F 1 "G5V-1" H 4470 2995 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5170 3010 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4040 3040 50  0001 C CNN
	1    4040 3040
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F82DE9F
P 5400 3020
AR Path="/5F82DE9F" Ref="J?"  Part="1" 
AR Path="/5F828554/5F82DE9F" Ref="OUT2"  Part="1" 
F 0 "OUT2" H 5480 3062 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 5480 2971 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 5400 3020 50  0001 C CNN
F 3 "~" H 5400 3020 50  0001 C CNN
	1    5400 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	4340 2740 4780 2740
Wire Wire Line
	4780 2740 4780 3020
Wire Wire Line
	4780 3020 5200 3020
Wire Wire Line
	4140 2740 4140 2630
Wire Wire Line
	4140 2630 4860 2630
Wire Wire Line
	4860 2630 4860 2920
Wire Wire Line
	4860 2920 5200 2920
Wire Wire Line
	5200 3120 4780 3120
Wire Wire Line
	4780 3120 4780 3380
Wire Wire Line
	4780 3380 4240 3380
Wire Wire Line
	4240 3380 4240 3340
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F82F8ED
P 5400 4640
AR Path="/5F82F8ED" Ref="J?"  Part="1" 
AR Path="/5F828554/5F82F8ED" Ref="OUT3"  Part="1" 
F 0 "OUT3" H 5480 4682 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 5480 4591 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 5400 4640 50  0001 C CNN
F 3 "~" H 5400 4640 50  0001 C CNN
	1    5400 4640
	1    0    0    -1  
$EndComp
Wire Wire Line
	4340 4360 4780 4360
Wire Wire Line
	4780 4360 4780 4640
Wire Wire Line
	4780 4640 5200 4640
Wire Wire Line
	4140 4360 4140 4250
Wire Wire Line
	4140 4250 4860 4250
Wire Wire Line
	4860 4250 4860 4540
Wire Wire Line
	4860 4540 5200 4540
Wire Wire Line
	5200 4740 4780 4740
Wire Wire Line
	4780 4740 4780 5000
Wire Wire Line
	4780 5000 4240 5000
Wire Wire Line
	4240 5000 4240 4960
$Comp
L Relay:G5V-1 K?
U 1 1 5F837BCD
P 4040 6560
AR Path="/5F837BCD" Ref="K?"  Part="1" 
AR Path="/5F828554/5F837BCD" Ref="K4"  Part="1" 
F 0 "K4" H 4470 6606 50  0000 L CNN
F 1 "G5V-1" H 4470 6515 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5170 6530 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4040 6560 50  0001 C CNN
	1    4040 6560
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F837BD3
P 5400 6540
AR Path="/5F837BD3" Ref="J?"  Part="1" 
AR Path="/5F828554/5F837BD3" Ref="OUT4"  Part="1" 
F 0 "OUT4" H 5480 6582 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 5480 6491 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 5400 6540 50  0001 C CNN
F 3 "~" H 5400 6540 50  0001 C CNN
	1    5400 6540
	1    0    0    -1  
$EndComp
Wire Wire Line
	4340 6260 4780 6260
Wire Wire Line
	4780 6260 4780 6540
Wire Wire Line
	4780 6540 5200 6540
Wire Wire Line
	4140 6260 4140 6150
Wire Wire Line
	4140 6150 4860 6150
Wire Wire Line
	4860 6150 4860 6440
Wire Wire Line
	4860 6440 5200 6440
Wire Wire Line
	5200 6640 4780 6640
Wire Wire Line
	4780 6640 4780 6900
Wire Wire Line
	4780 6900 4240 6900
Wire Wire Line
	4240 6900 4240 6860
$Comp
L Relay:G5V-1 K?
U 1 1 5F83DF60
P 8090 2700
AR Path="/5F83DF60" Ref="K?"  Part="1" 
AR Path="/5F828554/5F83DF60" Ref="K6"  Part="1" 
F 0 "K6" H 8520 2746 50  0000 L CNN
F 1 "G5V-1" H 8520 2655 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 9220 2670 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 8090 2700 50  0001 C CNN
	1    8090 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F83DF66
P 9450 2680
AR Path="/5F83DF66" Ref="J?"  Part="1" 
AR Path="/5F828554/5F83DF66" Ref="OUT6"  Part="1" 
F 0 "OUT6" H 9530 2722 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 9530 2631 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 9450 2680 50  0001 C CNN
F 3 "~" H 9450 2680 50  0001 C CNN
	1    9450 2680
	1    0    0    -1  
$EndComp
Wire Wire Line
	8390 2400 8830 2400
Wire Wire Line
	8830 2400 8830 2680
Wire Wire Line
	8830 2680 9250 2680
Wire Wire Line
	8190 2400 8190 2290
Wire Wire Line
	8190 2290 8910 2290
Wire Wire Line
	8910 2290 8910 2580
Wire Wire Line
	8910 2580 9250 2580
Wire Wire Line
	9250 2780 8830 2780
Wire Wire Line
	8830 2780 8830 3040
Wire Wire Line
	8830 3040 8290 3040
Wire Wire Line
	8290 3040 8290 3000
$Comp
L Relay:G5V-1 K?
U 1 1 5F83DF77
P 8100 1360
AR Path="/5F83DF77" Ref="K?"  Part="1" 
AR Path="/5F828554/5F83DF77" Ref="K5"  Part="1" 
F 0 "K5" H 8530 1406 50  0000 L CNN
F 1 "G5V-1" H 8530 1315 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 9230 1330 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 8100 1360 50  0001 C CNN
	1    8100 1360
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F83DF7D
P 9460 1340
AR Path="/5F83DF7D" Ref="J?"  Part="1" 
AR Path="/5F828554/5F83DF7D" Ref="OUT5"  Part="1" 
F 0 "OUT5" H 9540 1382 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 9540 1291 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 9460 1340 50  0001 C CNN
F 3 "~" H 9460 1340 50  0001 C CNN
	1    9460 1340
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 1060 8840 1060
Wire Wire Line
	8840 1060 8840 1340
Wire Wire Line
	8840 1340 9260 1340
Wire Wire Line
	8200 1060 8200 950 
Wire Wire Line
	8200 950  8920 950 
Wire Wire Line
	8920 950  8920 1240
Wire Wire Line
	8920 1240 9260 1240
Wire Wire Line
	9260 1440 8840 1440
Wire Wire Line
	8840 1440 8840 1700
Wire Wire Line
	8840 1700 8300 1700
Wire Wire Line
	8300 1700 8300 1660
$Comp
L Relay:G5V-1 K?
U 1 1 5F83DF8E
P 8130 3910
AR Path="/5F83DF8E" Ref="K?"  Part="1" 
AR Path="/5F828554/5F83DF8E" Ref="K7"  Part="1" 
F 0 "K7" H 8560 3956 50  0000 L CNN
F 1 "G5V-1" H 8560 3865 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 9260 3880 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 8130 3910 50  0001 C CNN
	1    8130 3910
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F83DF94
P 9490 3890
AR Path="/5F83DF94" Ref="J?"  Part="1" 
AR Path="/5F828554/5F83DF94" Ref="OUT7"  Part="1" 
F 0 "OUT7" H 9570 3932 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 9570 3841 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 9490 3890 50  0001 C CNN
F 3 "~" H 9490 3890 50  0001 C CNN
	1    9490 3890
	1    0    0    -1  
$EndComp
Wire Wire Line
	8430 3610 8870 3610
Wire Wire Line
	8870 3610 8870 3890
Wire Wire Line
	8870 3890 9290 3890
Wire Wire Line
	8230 3610 8230 3500
Wire Wire Line
	8230 3500 8950 3500
Wire Wire Line
	8950 3500 8950 3790
Wire Wire Line
	8950 3790 9290 3790
Wire Wire Line
	9290 3990 8870 3990
Wire Wire Line
	8870 3990 8870 4250
Wire Wire Line
	8870 4250 8330 4250
Wire Wire Line
	8330 4250 8330 4210
$Comp
L Relay:G5V-1 K?
U 1 1 5F8416CC
P 8150 5550
AR Path="/5F8416CC" Ref="K?"  Part="1" 
AR Path="/5F828554/5F8416CC" Ref="K8"  Part="1" 
F 0 "K8" H 8580 5596 50  0000 L CNN
F 1 "G5V-1" H 8580 5505 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 9280 5520 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 8150 5550 50  0001 C CNN
	1    8150 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F8416D2
P 9510 5530
AR Path="/5F8416D2" Ref="J?"  Part="1" 
AR Path="/5F828554/5F8416D2" Ref="OUT8"  Part="1" 
F 0 "OUT8" H 9590 5572 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 9590 5481 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-3-5.08_1x03_P5.08mm_Horizontal" H 9510 5530 50  0001 C CNN
F 3 "~" H 9510 5530 50  0001 C CNN
	1    9510 5530
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 5250 8890 5250
Wire Wire Line
	8890 5250 8890 5530
Wire Wire Line
	8890 5530 9310 5530
Wire Wire Line
	8250 5250 8250 5140
Wire Wire Line
	8250 5140 8970 5140
Wire Wire Line
	8970 5140 8970 5430
Wire Wire Line
	8970 5430 9310 5430
Wire Wire Line
	9310 5630 8890 5630
Wire Wire Line
	8890 5630 8890 5890
Wire Wire Line
	8890 5890 8350 5890
Wire Wire Line
	8350 5890 8350 5850
$Comp
L Transistor_Array:ULN2803A U1
U 1 1 5F8A0ABA
P 2080 4090
F 0 "U1" H 2080 4657 50  0000 C CNN
F 1 "ULN2803A" H 2080 4566 50  0000 C CNN
F 2 "Package_DIP:DIP-18_W7.62mm" H 2130 3440 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2803a.pdf" H 2180 3890 50  0001 C CNN
	1    2080 4090
	1    0    0    -1  
$EndComp
Wire Wire Line
	2480 3890 2710 3890
Wire Wire Line
	2480 3990 2710 3990
Wire Wire Line
	2480 4090 2710 4090
Wire Wire Line
	2480 4190 2710 4190
Wire Wire Line
	2480 4290 2710 4290
Wire Wire Line
	2480 4390 2710 4390
Wire Wire Line
	2480 4490 2710 4490
Wire Wire Line
	2480 4590 2710 4590
Wire Wire Line
	2080 4790 2080 4860
$Comp
L power:GND #PWR01
U 1 1 5F8E5FA1
P 2080 4860
F 0 "#PWR01" H 2080 4610 50  0001 C CNN
F 1 "GND" H 2085 4687 50  0000 C CNN
F 2 "" H 2080 4860 50  0001 C CNN
F 3 "" H 2080 4860 50  0001 C CNN
	1    2080 4860
	1    0    0    -1  
$EndComp
Wire Wire Line
	2480 3790 2580 3790
Wire Wire Line
	3840 6260 3840 6200
Wire Wire Line
	7950 5250 7950 5180
Wire Wire Line
	7930 3610 7930 3550
Wire Wire Line
	7900 1060 7900 990 
Wire Wire Line
	7890 2400 7890 2330
Text Label 2710 3890 2    50   ~ 0
RL1
Text Label 2710 3990 2    50   ~ 0
RL2
Text Label 2710 4090 2    50   ~ 0
RL3
Text Label 2710 4190 2    50   ~ 0
RL4
Text Label 2710 4290 2    50   ~ 0
RL5
Text Label 2710 4390 2    50   ~ 0
RL6
Text Label 2710 4490 2    50   ~ 0
RL7
Text Label 2710 4590 2    50   ~ 0
RL8
Wire Wire Line
	3820 1840 3820 1910
Wire Wire Line
	3820 1980 3620 1980
Wire Wire Line
	3840 3340 3840 3400
Wire Wire Line
	3840 3480 3640 3480
Wire Wire Line
	3840 4960 3840 5020
Wire Wire Line
	3840 5100 3640 5100
Wire Wire Line
	3840 6860 3840 6920
Wire Wire Line
	3840 7000 3640 7000
Wire Wire Line
	7950 5850 7950 5900
Wire Wire Line
	7950 5990 7750 5990
Wire Wire Line
	7930 4210 7930 4270
Wire Wire Line
	7930 4350 7730 4350
Wire Wire Line
	7900 1660 7900 1710
Wire Wire Line
	7900 1800 7700 1800
Wire Wire Line
	7890 3000 7890 3050
Wire Wire Line
	7890 3140 7690 3140
Text Label 3620 1980 0    50   ~ 0
RL1
Text Label 3640 3480 0    50   ~ 0
RL2
Text Label 3640 5100 0    50   ~ 0
RL3
Text Label 3640 7000 0    50   ~ 0
RL4
Text Label 7690 3140 0    50   ~ 0
RL6
Text Label 7700 1800 0    50   ~ 0
RL5
Text Label 7730 4350 0    50   ~ 0
RL7
Text Label 7750 5990 0    50   ~ 0
RL8
$Comp
L Device:R R1
U 1 1 5F92A303
P 3240 1380
F 0 "R1" H 3310 1426 50  0000 L CNN
F 1 "330R" H 3310 1335 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 3170 1380 50  0001 C CNN
F 3 "~" H 3240 1380 50  0001 C CNN
	1    3240 1380
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F92AF61
P 3240 1710
F 0 "D1" V 3279 1593 50  0000 R CNN
F 1 "LED" V 3188 1593 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3240 1710 50  0001 C CNN
F 3 "~" H 3240 1710 50  0001 C CNN
	1    3240 1710
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3240 1530 3240 1560
Wire Wire Line
	3240 1230 3240 1190
Wire Wire Line
	3240 1190 3820 1190
Wire Wire Line
	3820 1910 3240 1910
Wire Wire Line
	3240 1860 3240 1910
Connection ~ 3820 1910
Wire Wire Line
	3820 1910 3820 1980
$Comp
L Device:LED D2
U 1 1 5F956067
P 3260 3200
F 0 "D2" V 3299 3083 50  0000 R CNN
F 1 "LED" V 3208 3083 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3260 3200 50  0001 C CNN
F 3 "~" H 3260 3200 50  0001 C CNN
	1    3260 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3260 3020 3260 3050
Wire Wire Line
	3260 2720 3260 2680
Wire Wire Line
	3840 3400 3260 3400
Wire Wire Line
	3260 3350 3260 3400
Wire Wire Line
	3260 2680 3840 2680
Connection ~ 3840 3400
Wire Wire Line
	3840 3400 3840 3480
$Comp
L Device:R R3
U 1 1 5F962594
P 3260 4490
F 0 "R3" H 3330 4536 50  0000 L CNN
F 1 "330R" H 3330 4445 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 3190 4490 50  0001 C CNN
F 3 "~" H 3260 4490 50  0001 C CNN
	1    3260 4490
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5F96259A
P 3260 4820
F 0 "D3" V 3299 4703 50  0000 R CNN
F 1 "LED" V 3208 4703 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3260 4820 50  0001 C CNN
F 3 "~" H 3260 4820 50  0001 C CNN
	1    3260 4820
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3260 4640 3260 4670
Wire Wire Line
	3260 4340 3260 4300
Wire Wire Line
	3260 4300 3840 4300
Wire Wire Line
	3840 5020 3260 5020
Wire Wire Line
	3260 4970 3260 5020
Connection ~ 3840 5020
Wire Wire Line
	3840 5020 3840 5100
$Comp
L Device:R R4
U 1 1 5F9692F5
P 3260 6390
F 0 "R4" H 3330 6436 50  0000 L CNN
F 1 "330R" H 3330 6345 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 3190 6390 50  0001 C CNN
F 3 "~" H 3260 6390 50  0001 C CNN
	1    3260 6390
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 5F9692FB
P 3260 6720
F 0 "D4" V 3299 6603 50  0000 R CNN
F 1 "LED" V 3208 6603 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3260 6720 50  0001 C CNN
F 3 "~" H 3260 6720 50  0001 C CNN
	1    3260 6720
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3260 6540 3260 6570
Wire Wire Line
	3260 6240 3260 6200
Wire Wire Line
	3260 6200 3840 6200
Wire Wire Line
	3840 6920 3260 6920
Wire Wire Line
	3260 6870 3260 6920
Connection ~ 3840 6200
Connection ~ 3840 6920
Wire Wire Line
	3840 6920 3840 7000
$Comp
L Device:R R6
U 1 1 5F978904
P 7310 2520
F 0 "R6" H 7380 2566 50  0000 L CNN
F 1 "330R" H 7380 2475 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 7240 2520 50  0001 C CNN
F 3 "~" H 7310 2520 50  0001 C CNN
	1    7310 2520
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 5F97890A
P 7310 2850
F 0 "D6" V 7349 2733 50  0000 R CNN
F 1 "LED" V 7258 2733 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7310 2850 50  0001 C CNN
F 3 "~" H 7310 2850 50  0001 C CNN
	1    7310 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7310 2670 7310 2700
Wire Wire Line
	7310 2370 7310 2330
Wire Wire Line
	7310 2330 7890 2330
Wire Wire Line
	7890 3050 7310 3050
Wire Wire Line
	7310 3000 7310 3050
Connection ~ 7890 2330
Wire Wire Line
	7890 2330 7890 2290
Connection ~ 7890 3050
Wire Wire Line
	7890 3050 7890 3140
$Comp
L Device:R R5
U 1 1 5F980BB1
P 7320 1180
F 0 "R5" H 7390 1226 50  0000 L CNN
F 1 "330R" H 7390 1135 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 7250 1180 50  0001 C CNN
F 3 "~" H 7320 1180 50  0001 C CNN
	1    7320 1180
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 5F980BB7
P 7320 1510
F 0 "D5" V 7359 1393 50  0000 R CNN
F 1 "LED" V 7268 1393 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7320 1510 50  0001 C CNN
F 3 "~" H 7320 1510 50  0001 C CNN
	1    7320 1510
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7320 1330 7320 1360
Wire Wire Line
	7320 1030 7320 990 
Wire Wire Line
	7320 990  7900 990 
Wire Wire Line
	7900 1710 7320 1710
Wire Wire Line
	7320 1660 7320 1710
Connection ~ 7900 990 
Wire Wire Line
	7900 990  7900 920 
Connection ~ 7900 1710
Wire Wire Line
	7900 1710 7900 1800
$Comp
L Device:R R7
U 1 1 5F989102
P 7350 3740
F 0 "R7" H 7420 3786 50  0000 L CNN
F 1 "330R" H 7420 3695 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 7280 3740 50  0001 C CNN
F 3 "~" H 7350 3740 50  0001 C CNN
	1    7350 3740
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D7
U 1 1 5F989108
P 7350 4070
F 0 "D7" V 7389 3953 50  0000 R CNN
F 1 "LED" V 7298 3953 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7350 4070 50  0001 C CNN
F 3 "~" H 7350 4070 50  0001 C CNN
	1    7350 4070
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 3890 7350 3920
Wire Wire Line
	7350 3590 7350 3550
Wire Wire Line
	7350 3550 7930 3550
Wire Wire Line
	7930 4270 7350 4270
Wire Wire Line
	7350 4220 7350 4270
Connection ~ 7930 3550
Wire Wire Line
	7930 3550 7930 3480
Connection ~ 7930 4270
Wire Wire Line
	7930 4270 7930 4350
$Comp
L Device:R R8
U 1 1 5F992486
P 7370 5370
F 0 "R8" H 7440 5416 50  0000 L CNN
F 1 "330R" H 7440 5325 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 7300 5370 50  0001 C CNN
F 3 "~" H 7370 5370 50  0001 C CNN
	1    7370 5370
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D8
U 1 1 5F99248C
P 7370 5700
F 0 "D8" V 7409 5583 50  0000 R CNN
F 1 "LED" V 7318 5583 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7370 5700 50  0001 C CNN
F 3 "~" H 7370 5700 50  0001 C CNN
	1    7370 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7370 5520 7370 5550
Wire Wire Line
	7370 5220 7370 5180
Wire Wire Line
	7370 5180 7950 5180
Wire Wire Line
	7950 5900 7370 5900
Wire Wire Line
	7370 5850 7370 5900
Connection ~ 7950 5180
Wire Wire Line
	7950 5180 7950 5070
Connection ~ 7950 5900
Wire Wire Line
	7950 5900 7950 5990
Wire Wire Line
	3840 2740 3840 2680
Wire Wire Line
	3840 2680 3840 2610
Connection ~ 3840 2680
$Comp
L Relay:G5V-1 K?
U 1 1 5F82F8E7
P 4040 4660
AR Path="/5F82F8E7" Ref="K?"  Part="1" 
AR Path="/5F828554/5F82F8E7" Ref="K3"  Part="1" 
F 0 "K3" H 4470 4706 50  0000 L CNN
F 1 "G5V-1" H 4470 4615 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5170 4630 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4040 4660 50  0001 C CNN
	1    4040 4660
	1    0    0    -1  
$EndComp
Wire Wire Line
	3840 4360 3840 4300
Wire Wire Line
	3840 4300 3840 4250
Connection ~ 3840 4300
Wire Wire Line
	3840 6140 3840 6200
Wire Wire Line
	3820 1240 3820 1190
Wire Wire Line
	3820 1190 3820 1140
Connection ~ 3820 1190
$Comp
L power:+5V #PWR02
U 1 1 5F9ED1CA
P 2580 3790
F 0 "#PWR02" H 2580 3640 50  0001 C CNN
F 1 "+5V" H 2595 3963 50  0000 C CNN
F 2 "" H 2580 3790 50  0001 C CNN
F 3 "" H 2580 3790 50  0001 C CNN
	1    2580 3790
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5F9EE4CA
P 3820 1140
F 0 "#PWR03" H 3820 990 50  0001 C CNN
F 1 "+5V" H 3835 1313 50  0000 C CNN
F 2 "" H 3820 1140 50  0001 C CNN
F 3 "" H 3820 1140 50  0001 C CNN
	1    3820 1140
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5F9EF0F8
P 3840 2610
F 0 "#PWR04" H 3840 2460 50  0001 C CNN
F 1 "+5V" H 3855 2783 50  0000 C CNN
F 2 "" H 3840 2610 50  0001 C CNN
F 3 "" H 3840 2610 50  0001 C CNN
	1    3840 2610
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5F9F9BE2
P 3840 4250
F 0 "#PWR05" H 3840 4100 50  0001 C CNN
F 1 "+5V" H 3855 4423 50  0000 C CNN
F 2 "" H 3840 4250 50  0001 C CNN
F 3 "" H 3840 4250 50  0001 C CNN
	1    3840 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5FA02EDB
P 3840 6140
F 0 "#PWR06" H 3840 5990 50  0001 C CNN
F 1 "+5V" H 3855 6313 50  0000 C CNN
F 2 "" H 3840 6140 50  0001 C CNN
F 3 "" H 3840 6140 50  0001 C CNN
	1    3840 6140
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 5FA0C15E
P 7950 5070
F 0 "#PWR010" H 7950 4920 50  0001 C CNN
F 1 "+5V" H 7965 5243 50  0000 C CNN
F 2 "" H 7950 5070 50  0001 C CNN
F 3 "" H 7950 5070 50  0001 C CNN
	1    7950 5070
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR09
U 1 1 5FA1515E
P 7930 3480
F 0 "#PWR09" H 7930 3330 50  0001 C CNN
F 1 "+5V" H 7945 3653 50  0000 C CNN
F 2 "" H 7930 3480 50  0001 C CNN
F 3 "" H 7930 3480 50  0001 C CNN
	1    7930 3480
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR07
U 1 1 5FA1E083
P 7900 920
F 0 "#PWR07" H 7900 770 50  0001 C CNN
F 1 "+5V" H 7915 1093 50  0000 C CNN
F 2 "" H 7900 920 50  0001 C CNN
F 3 "" H 7900 920 50  0001 C CNN
	1    7900 920 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR08
U 1 1 5FA27562
P 7890 2290
F 0 "#PWR08" H 7890 2140 50  0001 C CNN
F 1 "+5V" H 7905 2463 50  0000 C CNN
F 2 "" H 7890 2290 50  0001 C CNN
F 3 "" H 7890 2290 50  0001 C CNN
	1    7890 2290
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F956061
P 3260 2870
F 0 "R2" H 3330 2916 50  0000 L CNN
F 1 "330R" H 3330 2825 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 3190 2870 50  0001 C CNN
F 3 "~" H 3260 2870 50  0001 C CNN
	1    3260 2870
	1    0    0    -1  
$EndComp
Entry Wire Line
	1290 4490 1390 4590
Entry Wire Line
	1290 4390 1390 4490
Entry Wire Line
	1290 4290 1390 4390
Entry Wire Line
	1290 4190 1390 4290
Entry Wire Line
	1290 4090 1390 4190
Entry Wire Line
	1290 3990 1390 4090
Entry Wire Line
	1290 3890 1390 3990
Entry Wire Line
	1290 3790 1390 3890
Wire Wire Line
	1680 4590 1390 4590
Wire Wire Line
	1390 4490 1680 4490
Wire Wire Line
	1390 4390 1680 4390
Wire Wire Line
	1390 4290 1680 4290
Wire Wire Line
	1390 4190 1680 4190
Wire Wire Line
	1390 4090 1680 4090
Wire Wire Line
	1390 3990 1680 3990
Wire Wire Line
	1680 3890 1390 3890
Wire Bus Line
	1290 3680 1070 3680
Text Label 1420 4290 0    50   ~ 0
OUT1
Text Label 1420 4390 0    50   ~ 0
OUT2
Text Label 1420 4590 0    50   ~ 0
OUT3
Text Label 1420 4490 0    50   ~ 0
OUT4
Text Label 1420 4190 0    50   ~ 0
OUT5
Text Label 1420 4090 0    50   ~ 0
OUT6
Text Label 1420 3990 0    50   ~ 0
OUT7
Text Label 1420 3890 0    50   ~ 0
OUT8
Text HLabel 1070 3680 0    50   Input ~ 0
OUT[1..8]
Wire Bus Line
	1290 3680 1290 4490
$EndSCHEMATC
