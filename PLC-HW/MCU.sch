EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R9
U 1 1 5FAF6BA5
P 2270 5220
F 0 "R9" H 2340 5266 50  0000 L CNN
F 1 "4k7" H 2340 5175 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 2200 5220 50  0001 C CNN
F 3 "~" H 2270 5220 50  0001 C CNN
	1    2270 5220
	1    0    0    -1  
$EndComp
Wire Wire Line
	2270 5370 2270 5620
$Comp
L Switch:SW_Push SW1
U 1 1 5FAF9B14
P 2270 5960
F 0 "SW1" V 2316 5912 50  0000 R CNN
F 1 "SW_Push" V 2225 5912 50  0000 R CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H5mm" H 2270 6160 50  0001 C CNN
F 3 "~" H 2270 6160 50  0001 C CNN
	1    2270 5960
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 5FAFB6D4
P 2760 5970
F 0 "C4" H 2875 6016 50  0000 L CNN
F 1 "100n" H 2875 5925 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 2798 5820 50  0001 C CNN
F 3 "~" H 2760 5970 50  0001 C CNN
	1    2760 5970
	1    0    0    -1  
$EndComp
Wire Wire Line
	2270 5760 2270 5620
Wire Wire Line
	2270 5620 2280 5620
Connection ~ 2270 5620
Wire Wire Line
	2270 6160 2270 6280
Wire Wire Line
	2270 6280 2760 6280
Wire Wire Line
	2760 6280 2760 6120
Wire Wire Line
	2760 5820 2760 5620
Wire Wire Line
	2270 5620 2760 5620
Connection ~ 2760 5620
Wire Wire Line
	2270 6280 2270 6390
Connection ~ 2270 6280
Wire Wire Line
	2270 5070 2270 4980
$Comp
L power:+5V #PWR011
U 1 1 5FB0074C
P 2270 4980
F 0 "#PWR011" H 2270 4830 50  0001 C CNN
F 1 "+5V" H 2285 5153 50  0000 C CNN
F 2 "" H 2270 4980 50  0001 C CNN
F 3 "" H 2270 4980 50  0001 C CNN
	1    2270 4980
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5FB00F72
P 2270 6390
F 0 "#PWR012" H 2270 6140 50  0001 C CNN
F 1 "GND" H 2275 6217 50  0000 C CNN
F 2 "" H 2270 6390 50  0001 C CNN
F 3 "" H 2270 6390 50  0001 C CNN
	1    2270 6390
	1    0    0    -1  
$EndComp
Text Label 3000 5620 2    50   ~ 0
MCLR
Wire Wire Line
	2760 5620 3000 5620
$Comp
L Device:C C1
U 1 1 5FADF40D
P 4070 5760
F 0 "C1" H 3820 5810 50  0000 L CNN
F 1 "100n" H 3770 5720 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 4108 5610 50  0001 C CNN
F 3 "~" H 4070 5760 50  0001 C CNN
	1    4070 5760
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5FADFEDD
P 4390 5760
F 0 "C2" H 4508 5806 50  0000 L CNN
F 1 "100u" H 4508 5715 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D7.5mm_P2.50mm" H 4428 5610 50  0001 C CNN
F 3 "~" H 4390 5760 50  0001 C CNN
	1    4390 5760
	1    0    0    -1  
$EndComp
Wire Wire Line
	4070 5610 4230 5610
Wire Wire Line
	4070 5910 4230 5910
Connection ~ 4230 5910
Wire Wire Line
	4230 5910 4390 5910
Wire Wire Line
	4230 5610 4230 5550
Connection ~ 4230 5610
Wire Wire Line
	4230 5610 4390 5610
Wire Wire Line
	4230 5910 4230 5970
$Comp
L power:GND #PWR0104
U 1 1 5FAE12E9
P 4230 5970
F 0 "#PWR0104" H 4230 5720 50  0001 C CNN
F 1 "GND" H 4235 5797 50  0000 C CNN
F 2 "" H 4230 5970 50  0001 C CNN
F 3 "" H 4230 5970 50  0001 C CNN
	1    4230 5970
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5FAE1C08
P 4230 5550
F 0 "#PWR0105" H 4230 5400 50  0001 C CNN
F 1 "+5V" H 4245 5723 50  0000 C CNN
F 2 "" H 4230 5550 50  0001 C CNN
F 3 "" H 4230 5550 50  0001 C CNN
	1    4230 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FADD14D
P 4950 4920
F 0 "#PWR0102" H 4950 4670 50  0001 C CNN
F 1 "GND" H 4955 4747 50  0000 C CNN
F 2 "" H 4950 4920 50  0001 C CNN
F 3 "" H 4950 4920 50  0001 C CNN
	1    4950 4920
	1    0    0    -1  
$EndComp
Text Label 3780 2740 2    50   ~ 0
OUT1
Text Label 3780 2840 2    50   ~ 0
OUT2
Text Label 3780 2940 2    50   ~ 0
OUT3
Text Label 3780 3140 2    50   ~ 0
OUT5
Text Label 3780 3340 2    50   ~ 0
OUT7
Text Label 3780 3440 2    50   ~ 0
OUT8
Wire Wire Line
	3850 2040 3450 2040
Text Label 3450 2040 0    50   ~ 0
MCLR
Entry Wire Line
	3500 3340 3400 3240
Entry Wire Line
	3500 3240 3400 3140
Entry Wire Line
	3500 3140 3400 3040
Entry Wire Line
	3500 3040 3400 2940
Entry Wire Line
	3500 2940 3400 2840
Entry Wire Line
	3500 2840 3400 2740
Entry Wire Line
	3500 2740 3400 2640
Wire Wire Line
	3850 2740 3500 2740
Wire Wire Line
	3850 2840 3500 2840
Wire Wire Line
	3850 2940 3500 2940
Wire Wire Line
	3850 3040 3500 3040
Wire Wire Line
	3850 3140 3500 3140
Wire Wire Line
	3850 3240 3500 3240
Wire Wire Line
	3850 3340 3500 3340
Wire Wire Line
	3850 3440 3500 3440
Text HLabel 2470 2620 0    50   Output ~ 0
OUT[1..8]
$Comp
L Connector:Conn_01x06_Male J9
U 1 1 5FB1F3D1
P 5480 5660
F 0 "J9" H 5588 6041 50  0000 C CNN
F 1 "PICKIT" H 5588 5950 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5480 5660 50  0001 C CNN
F 3 "~" H 5480 5660 50  0001 C CNN
	1    5480 5660
	1    0    0    -1  
$EndComp
Wire Wire Line
	5680 5560 6210 5560
Wire Wire Line
	5680 5660 6210 5660
Text Label 5980 5460 2    50   ~ 0
MCLR
Wire Wire Line
	5680 5460 5980 5460
$Comp
L power:+5V #PWR013
U 1 1 5FB26A4E
P 6210 5560
F 0 "#PWR013" H 6210 5410 50  0001 C CNN
F 1 "+5V" H 6225 5733 50  0000 C CNN
F 2 "" H 6210 5560 50  0001 C CNN
F 3 "" H 6210 5560 50  0001 C CNN
	1    6210 5560
	1    0    0    -1  
$EndComp
Wire Wire Line
	6210 5660 6210 5710
$Comp
L power:GND #PWR014
U 1 1 5FB283B2
P 6210 5710
F 0 "#PWR014" H 6210 5460 50  0001 C CNN
F 1 "GND" H 6215 5537 50  0000 C CNN
F 2 "" H 6210 5710 50  0001 C CNN
F 3 "" H 6210 5710 50  0001 C CNN
	1    6210 5710
	1    0    0    -1  
$EndComp
Wire Wire Line
	5680 5760 5980 5760
Wire Wire Line
	5680 5860 5980 5860
Text Label 5980 5760 2    50   ~ 0
PGD
Text Label 5980 5860 2    50   ~ 0
PGC
NoConn ~ 5680 5960
Text Label 3780 3240 2    50   ~ 0
OUT6
Text Label 3780 3040 2    50   ~ 0
OUT4
$Comp
L MCU_Microchip_PIC16:PIC16F887-IP U2
U 1 1 6001E6EA
P 4950 3240
F 0 "U2" H 4050 4720 50  0000 C CNN
F 1 "PIC16F887-IP" H 4270 4640 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 4950 3240 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/41291D.pdf" H 4950 3240 50  0001 C CNN
	1    4950 3240
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4740 4950 4830
Wire Wire Line
	4850 4740 4850 4830
Wire Wire Line
	4850 4830 4950 4830
Connection ~ 4950 4830
Wire Wire Line
	4950 4830 4950 4920
Wire Wire Line
	4850 1740 4950 1740
Connection ~ 4950 1740
$Comp
L power:+5V #PWR0101
U 1 1 60025644
P 4950 1440
F 0 "#PWR0101" H 4950 1290 50  0001 C CNN
F 1 "+5V" H 4965 1613 50  0000 C CNN
F 2 "" H 4950 1440 50  0001 C CNN
F 3 "" H 4950 1440 50  0001 C CNN
	1    4950 1440
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1440 4950 1740
Text HLabel 7820 2410 2    50   Input ~ 0
IN[1..8]
Wire Bus Line
	7820 2410 6880 2410
Entry Wire Line
	6880 3340 6780 3440
Entry Wire Line
	6880 3240 6780 3340
Entry Wire Line
	6880 3140 6780 3240
Entry Wire Line
	6880 3040 6780 3140
Entry Wire Line
	6880 2940 6780 3040
Entry Wire Line
	6880 2840 6780 2940
Entry Wire Line
	6880 2740 6780 2840
Entry Wire Line
	6880 2640 6780 2740
Text Label 6450 3340 2    50   ~ 0
PGC
Text Label 6450 3440 2    50   ~ 0
PGD
Text Label 6650 2740 2    50   ~ 0
IN8
Text Label 6650 2840 2    50   ~ 0
IN7
Text Label 6650 2940 2    50   ~ 0
IN6
Text Label 6650 3040 2    50   ~ 0
IN5
Text Label 6650 3140 2    50   ~ 0
IN4
Text Label 6650 3240 2    50   ~ 0
IN3
Text Label 6650 3340 2    50   ~ 0
IN2
Text Label 6650 3440 2    50   ~ 0
IN1
Wire Wire Line
	6050 3440 6780 3440
Wire Wire Line
	6050 3340 6780 3340
Wire Wire Line
	6050 3240 6780 3240
Wire Wire Line
	6050 3140 6780 3140
Wire Wire Line
	6050 3040 6780 3040
Wire Wire Line
	6050 2940 6780 2940
Wire Wire Line
	6050 2840 6780 2840
Wire Wire Line
	6050 2740 6780 2740
Wire Bus Line
	3400 2620 2470 2620
Entry Wire Line
	3500 3440 3400 3340
Wire Wire Line
	9640 1700 9850 1700
Wire Wire Line
	9640 1800 9850 1800
$Comp
L Connector:Screw_Terminal_01x04 J4
U 1 1 600D5AE7
P 10050 1700
F 0 "J4" H 10130 1692 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 10130 1601 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-4-5.08_1x04_P5.08mm_Horizontal" H 10050 1700 50  0001 C CNN
F 3 "~" H 10050 1700 50  0001 C CNN
	1    10050 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR049
U 1 1 600D969C
P 9850 1900
F 0 "#PWR049" H 9850 1650 50  0001 C CNN
F 1 "GND" H 9855 1727 50  0000 C CNN
F 2 "" H 9850 1900 50  0001 C CNN
F 3 "" H 9850 1900 50  0001 C CNN
	1    9850 1900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR048
U 1 1 600DA264
P 9850 1600
F 0 "#PWR048" H 9850 1450 50  0001 C CNN
F 1 "+5V" H 9865 1773 50  0000 C CNN
F 2 "" H 9850 1600 50  0001 C CNN
F 3 "" H 9850 1600 50  0001 C CNN
	1    9850 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9640 1240 9350 1240
Wire Wire Line
	9640 2330 9350 2330
$Comp
L Device:D D22
U 1 1 600E7832
P 9350 1090
F 0 "D22" V 9304 1169 50  0000 L CNN
F 1 "1N4001" V 9395 1169 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 9350 1090 50  0001 C CNN
F 3 "~" H 9350 1090 50  0001 C CNN
	1    9350 1090
	0    1    1    0   
$EndComp
Connection ~ 9350 1240
Wire Wire Line
	9350 1240 9110 1240
$Comp
L Device:D D24
U 1 1 600FC59C
P 9350 2180
F 0 "D24" V 9280 1960 50  0000 L CNN
F 1 "1N4001" V 9350 1810 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 9350 2180 50  0001 C CNN
F 3 "~" H 9350 2180 50  0001 C CNN
	1    9350 2180
	0    1    1    0   
$EndComp
Connection ~ 9350 2330
Wire Wire Line
	9350 2330 9110 2330
$Comp
L Device:D D25
U 1 1 600FD408
P 9350 2480
F 0 "D25" V 9304 2559 50  0000 L CNN
F 1 "1N4001" V 9395 2559 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 9350 2480 50  0001 C CNN
F 3 "~" H 9350 2480 50  0001 C CNN
	1    9350 2480
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR044
U 1 1 600FE379
P 9350 940
F 0 "#PWR044" H 9350 790 50  0001 C CNN
F 1 "+5V" H 9365 1113 50  0000 C CNN
F 2 "" H 9350 940 50  0001 C CNN
F 3 "" H 9350 940 50  0001 C CNN
	1    9350 940 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR046
U 1 1 600FEA2D
P 9350 2030
F 0 "#PWR046" H 9350 1880 50  0001 C CNN
F 1 "+5V" H 9365 2203 50  0000 C CNN
F 2 "" H 9350 2030 50  0001 C CNN
F 3 "" H 9350 2030 50  0001 C CNN
	1    9350 2030
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR045
U 1 1 600FF42C
P 9350 1540
F 0 "#PWR045" H 9350 1290 50  0001 C CNN
F 1 "GND" H 9355 1367 50  0000 C CNN
F 2 "" H 9350 1540 50  0001 C CNN
F 3 "" H 9350 1540 50  0001 C CNN
	1    9350 1540
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR047
U 1 1 600FF73A
P 9350 2630
F 0 "#PWR047" H 9350 2380 50  0001 C CNN
F 1 "GND" H 9355 2457 50  0000 C CNN
F 2 "" H 9350 2630 50  0001 C CNN
F 3 "" H 9350 2630 50  0001 C CNN
	1    9350 2630
	1    0    0    -1  
$EndComp
$Comp
L Device:R R39
U 1 1 60100132
P 9640 2040
F 0 "R39" V 9640 1970 50  0000 L CNN
F 1 "100" V 9560 1960 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 9570 2040 50  0001 C CNN
F 3 "~" H 9640 2040 50  0001 C CNN
	1    9640 2040
	1    0    0    -1  
$EndComp
Wire Wire Line
	9640 2330 9640 2190
Wire Wire Line
	9640 1890 9640 1800
$Comp
L Device:R R38
U 1 1 60105332
P 9640 1510
F 0 "R38" V 9640 1450 50  0000 L CNN
F 1 "100" V 9560 1430 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 9570 1510 50  0001 C CNN
F 3 "~" H 9640 1510 50  0001 C CNN
	1    9640 1510
	1    0    0    -1  
$EndComp
Wire Wire Line
	9640 1700 9640 1660
Wire Wire Line
	9640 1360 9640 1240
Text Label 9110 1240 0    50   ~ 0
AN0
Text Label 9110 2330 0    50   ~ 0
AN1
Wire Wire Line
	6050 2040 6260 2040
Wire Wire Line
	6050 2140 6260 2140
Text Label 6260 2040 2    50   ~ 0
AN0
Text Label 6260 2140 2    50   ~ 0
AN1
Wire Wire Line
	6050 3940 6260 3940
Wire Wire Line
	6050 4040 6260 4040
Text Label 6260 4040 2    50   ~ 0
SDA
Text Label 6260 3940 2    50   ~ 0
SCL
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 601179CC
P 9310 4280
F 0 "J3" H 9282 4162 50  0000 R CNN
F 1 "Conn_01x04_Male" H 9282 4253 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9310 4280 50  0001 C CNN
F 3 "~" H 9310 4280 50  0001 C CNN
	1    9310 4280
	-1   0    0    1   
$EndComp
$Comp
L Device:R R37
U 1 1 60119D71
P 8790 3930
F 0 "R37" H 8860 3976 50  0000 L CNN
F 1 "4K7" H 8860 3885 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 8720 3930 50  0001 C CNN
F 3 "~" H 8790 3930 50  0001 C CNN
	1    8790 3930
	1    0    0    -1  
$EndComp
$Comp
L Device:R R36
U 1 1 6011B3C1
P 8460 4030
F 0 "R36" H 8530 4076 50  0000 L CNN
F 1 "4K7" H 8530 3985 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 8390 4030 50  0001 C CNN
F 3 "~" H 8460 4030 50  0001 C CNN
	1    8460 4030
	1    0    0    -1  
$EndComp
Wire Wire Line
	8460 4180 9110 4180
Wire Wire Line
	8790 4080 9110 4080
Wire Wire Line
	8790 3730 8790 3780
Wire Wire Line
	8460 3880 8460 3730
Wire Wire Line
	8460 3730 8790 3730
Text Label 9050 4080 2    50   ~ 0
SCL
Text Label 9050 4180 2    50   ~ 0
SDA
Wire Wire Line
	8320 4280 8320 3730
Wire Wire Line
	8320 3730 8460 3730
Wire Wire Line
	8320 4280 9110 4280
Connection ~ 8460 3730
Wire Wire Line
	9110 4380 9030 4380
Wire Wire Line
	9030 4380 9030 4450
$Comp
L power:GND #PWR043
U 1 1 6012EADE
P 9030 4450
F 0 "#PWR043" H 9030 4200 50  0001 C CNN
F 1 "GND" H 9035 4277 50  0000 C CNN
F 2 "" H 9030 4450 50  0001 C CNN
F 3 "" H 9030 4450 50  0001 C CNN
	1    9030 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8460 3730 8460 3640
$Comp
L power:+5V #PWR041
U 1 1 60131A53
P 8460 3640
F 0 "#PWR041" H 8460 3490 50  0001 C CNN
F 1 "+5V" H 8475 3813 50  0000 C CNN
F 2 "" H 8460 3640 50  0001 C CNN
F 3 "" H 8460 3640 50  0001 C CNN
	1    8460 3640
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4240 6260 4240
Wire Wire Line
	6050 4340 6260 4340
Text Label 6260 4340 2    50   ~ 0
RX
Text Label 6260 4240 2    50   ~ 0
TX
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 60136F9E
P 9300 5700
F 0 "J2" H 9408 5981 50  0000 C CNN
F 1 "Conn_01x03_Male" H 9408 5890 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9300 5700 50  0001 C CNN
F 3 "~" H 9300 5700 50  0001 C CNN
	1    9300 5700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR042
U 1 1 6013FF2E
P 8660 5800
F 0 "#PWR042" H 8660 5550 50  0001 C CNN
F 1 "GND" H 8665 5627 50  0000 C CNN
F 2 "" H 8660 5800 50  0001 C CNN
F 3 "" H 8660 5800 50  0001 C CNN
	1    8660 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8660 5800 9100 5800
Wire Wire Line
	9100 5700 8660 5700
Wire Wire Line
	9100 5600 8660 5600
Text Label 8660 5600 0    50   ~ 0
RX
Text Label 8660 5700 0    50   ~ 0
TX
Wire Wire Line
	6050 3640 6260 3640
Wire Wire Line
	6050 3740 6260 3740
Wire Wire Line
	6050 3840 6260 3840
Text Label 6260 3640 2    50   ~ 0
P0
Text Label 6260 3740 2    50   ~ 0
P1
Text Label 6260 3840 2    50   ~ 0
P2
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 60151D4B
P 7110 4670
F 0 "J1" H 7218 5051 50  0000 C CNN
F 1 "Conn_01x05_Male" H 7218 4960 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 7110 4670 50  0001 C CNN
F 3 "~" H 7110 4670 50  0001 C CNN
	1    7110 4670
	1    0    0    -1  
$EndComp
Wire Wire Line
	7310 4570 7800 4570
Wire Wire Line
	7310 4670 7800 4670
Wire Wire Line
	7310 4770 7800 4770
Wire Wire Line
	7310 4870 7390 4870
Wire Wire Line
	7390 4870 7390 4980
Wire Wire Line
	7310 4470 7610 4470
Wire Wire Line
	7610 4470 7610 4390
$Comp
L power:+5V #PWR040
U 1 1 60160E91
P 7610 4390
F 0 "#PWR040" H 7610 4240 50  0001 C CNN
F 1 "+5V" H 7625 4563 50  0000 C CNN
F 2 "" H 7610 4390 50  0001 C CNN
F 3 "" H 7610 4390 50  0001 C CNN
	1    7610 4390
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR039
U 1 1 60161BD1
P 7390 4980
F 0 "#PWR039" H 7390 4730 50  0001 C CNN
F 1 "GND" H 7395 4807 50  0000 C CNN
F 2 "" H 7390 4980 50  0001 C CNN
F 3 "" H 7390 4980 50  0001 C CNN
	1    7390 4980
	1    0    0    -1  
$EndComp
Text Label 7800 4570 2    50   ~ 0
P0
Text Label 7800 4670 2    50   ~ 0
P1
Text Label 7800 4770 2    50   ~ 0
P2
$Comp
L Connector:TestPoint TP1
U 1 1 60164A61
P 1340 3600
F 0 "TP1" H 1398 3718 50  0000 L CNN
F 1 "TestPoint" H 1398 3627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 1540 3600 50  0001 C CNN
F 3 "~" H 1540 3600 50  0001 C CNN
	1    1340 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1340 3600 1340 3820
Wire Wire Line
	1340 3820 1510 3820
$Comp
L power:GND #PWR050
U 1 1 60168A60
P 1510 3820
F 0 "#PWR050" H 1510 3570 50  0001 C CNN
F 1 "GND" H 1515 3647 50  0000 C CNN
F 2 "" H 1510 3820 50  0001 C CNN
F 3 "" H 1510 3820 50  0001 C CNN
	1    1510 3820
	1    0    0    -1  
$EndComp
$Comp
L Device:D D23
U 1 1 600E8584
P 9350 1390
F 0 "D23" V 9300 1160 50  0000 L CNN
F 1 "1N4001" V 9390 1010 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_AnodeUp" H 9350 1390 50  0001 C CNN
F 3 "~" H 9350 1390 50  0001 C CNN
	1    9350 1390
	0    1    1    0   
$EndComp
NoConn ~ 3850 3640
NoConn ~ 3850 3740
NoConn ~ 3850 3840
NoConn ~ 6050 4140
NoConn ~ 6050 2540
NoConn ~ 6050 2240
NoConn ~ 6050 2340
NoConn ~ 6050 2440
NoConn ~ 3850 2340
NoConn ~ 3850 2540
$Comp
L Mechanical:MountingHole H1
U 1 1 6022A137
P 1270 1860
F 0 "H1" H 1370 1906 50  0000 L CNN
F 1 "MountingHole" H 1370 1815 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1270 1860 50  0001 C CNN
F 3 "~" H 1270 1860 50  0001 C CNN
	1    1270 1860
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 6022A7F0
P 1280 2130
F 0 "H4" H 1380 2176 50  0000 L CNN
F 1 "MountingHole" H 1380 2085 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1280 2130 50  0001 C CNN
F 3 "~" H 1280 2130 50  0001 C CNN
	1    1280 2130
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 6022ACF4
P 1270 2400
F 0 "H2" H 1370 2446 50  0000 L CNN
F 1 "MountingHole" H 1370 2355 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1270 2400 50  0001 C CNN
F 3 "~" H 1270 2400 50  0001 C CNN
	1    1270 2400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 6022B0DF
P 1270 2670
F 0 "H3" H 1370 2716 50  0000 L CNN
F 1 "MountingHole" H 1370 2625 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1270 2670 50  0001 C CNN
F 3 "~" H 1270 2670 50  0001 C CNN
	1    1270 2670
	1    0    0    -1  
$EndComp
Wire Bus Line
	6880 2410 6880 3340
Wire Bus Line
	3400 2620 3400 3450
$EndSCHEMATC
