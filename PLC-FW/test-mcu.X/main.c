/*
 * File:   main.c
 * Author: hepek
 *
 * Created on November 24, 2020, 8:10 PM
 */

#include <xc.h>
#include <stdint.h>
#include "config.h"
#include "I2C_LCD.h"
#include "uart.h"
//#include <stdio.h>

void main(void) {
  I2C_Master_Init();
  UART_Init(9600);

  LCD_Init(0x4E); // Initialize LCD module with I2C address = 0x4E
  LCD_Set_Cursor(1, 1);
  LCD_Write_String("PLC");
  TRISD = 0x00;
  uint8_t counter=0;
  while(1)
  {
      if(PORTBbits.RB7){
        LCD_Set_Cursor(2, 1);
        LCD_Write_String("           ");
        LCD_Set_Cursor(2, 1);
        LCD_Write_String("Cekanje");
        counter++;
        UART_Write_Text("Detektovan objekat\n");
        PORTD=counter;
        while(PORTBbits.RB7);
        PORTD= 0x00;
      }
      else{
        LCD_Set_Cursor(2, 1);
        LCD_Write_String("Detektovan");
        while(!PORTBbits.RB7);
      }
  } 
  return;
}


/*#include "config.h"
#include <stdint.h>

const uint16_t delay=2000;

void main(void) {    
    init_oscillator();
    
    ANSEL = 0x00;
    ANSELH = 0x00;
    TRISD = 0x00;
    while(1){        
        PORTD = 0x80;
        __delay_ms(delay);
        PORTD = 0x40;
        __delay_ms(delay);
        PORTD = 0x20;
        __delay_ms(delay);
        PORTD = 0x10;
        __delay_ms(delay);
        PORTD = 0x01;
        __delay_ms(delay);
        PORTD = 0x02;
        __delay_ms(delay);
        PORTD = 0x08;
        __delay_ms(delay);
        PORTD = 0x04;
        __delay_ms(delay);
    }
}*/

/*

 */
