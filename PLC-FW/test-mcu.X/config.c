#include "config.h"

void init_oscillator(void){
    
    OSCCONbits.IRCF0 = 1; //Internal Oscillator Frequency is 8 MHz
    OSCCONbits.IRCF1 = 1;
    OSCCONbits.IRCF2 = 1;
    
    OSCCONbits.SCS = 0; //Internal oscillator is used for system clock

    while(!OSCCONbits.HTS);
}